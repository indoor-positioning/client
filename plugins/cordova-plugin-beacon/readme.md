## 安卓使用

### 设置硬件使用申明和权限

在AndroidManifest.xml文件mainfet节点下加入
```xml
<uses-feature android:name="android.hardware.bluetooth" android:required="true" />
<uses-feature android:name="android.hardware.bluetooth_le"  android:required="true"/>
```
和
```xml
<uses-permission android:name="android.permission.FOREGROUND_SERVICE"/>
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
<uses-permission android:name="android.permission.ACCESS_BACKGROUND_LOCATION" />
<uses-permission android:name="android.permission.BLUETOOTH"/>
<uses-permission android:name="android.permission.BLUETOOTH_ADMIN"/>
```

如果是Android 6以上的版本，需要在代码里面申请定位权限

### 申明BeaconService

在AndroidManifest.xml文件mainfet\application节点下加入
```xml
<service android:name="me.zxhm.plugin.beacon.BeaconService"
            android:enabled="true"
            android:exported="true"/>
```

### 加入Android Beacon Library引用

配置gradle

```Groovy
repositories {
    jcenter()
}   

dependencies {
    implementation 'org.altbeacon:android-beacon-library:2+'
}
```