var exec = require('cordova/exec');

exports.toast = function (arg0, success, error) {
    exec(success, error, 'Beacon', 'toast', [arg0]);
};

exports.getBeacons = function (success, error) {
    exec(success, error, 'Beacon', 'getBeacons');
};

exports.startService = function (success, error) {
    exec(success, error, 'Beacon', 'startService');
};

exports.stopService = function (success, error) {
    exec(success, error, 'Beacon', 'stopService');
};
