cordova.define('cordova/plugin_list', function(require, exports, module) {
  module.exports = [
    {
      "id": "cordova-plugin-beacon.Beacon",
      "file": "plugins/cordova-plugin-beacon/www/Beacon.js",
      "pluginId": "cordova-plugin-beacon",
      "clobbers": [
        "cordova.plugins.Beacon"
      ]
    }
  ];
  module.exports.metadata = {
    "cordova-plugin-whitelist": "1.3.5",
    "cordova-plugin-beacon": "0.0.1"
  };
});