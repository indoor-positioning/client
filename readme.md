# 室内定位Demo手机APP

室内定位Demo使用[Cordova](https://cordova.apache.org/)打包的跨平台APP

添加平台比如安卓

```
cordova platform add android
```

## 插件

自定义了支持Beacon设备的插件`my_plugins\Beacon`,添加

```
cordova plugin add .\my_plugins\Beacon\
```

配置见插件[说明文档](./my_plugins/Beacon/readme.md)

