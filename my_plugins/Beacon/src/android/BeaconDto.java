package me.zxhm.plugin.beacon;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BeaconDto {
    private String name;
    private String uuid;
    private String mac;
    private int rssi;
    private double avgRssi;
    private byte[] major;
    private byte[] minor;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    public double getAvgRssi() {
        return avgRssi;
    }

    public void setAvgRssi(double avgRssi) {
        this.avgRssi = avgRssi;
    }

    public byte[] getMajor() {
        return major;
    }

    public void setMajor(byte[] major) {
        this.major = major;
    }

    public byte[] getMinor() {
        return minor;
    }

    public void setMinor(byte[] minor) {
        this.minor = minor;
    }

    public JSONObject toJSON()
    {
        JSONObject obj = new JSONObject();
        try {
            obj.put("name",name);
            obj.put("uuid",uuid);
            obj.put("mac",mac);
            obj.put("rssi",rssi);
            obj.put("avgRssi",avgRssi);
            JSONArray majorArr = new JSONArray(major);
            obj.put("major",majorArr);
            JSONArray minorArr = new JSONArray(minor);
            obj.put("minor",minorArr);
        }
        catch (JSONException e)
        {}
        return obj;
    }
}
