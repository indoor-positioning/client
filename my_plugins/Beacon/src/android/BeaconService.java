package me.zxhm.plugin.beacon;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.MonitorNotifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class BeaconService extends Service {
    public static final String TAG = "BeaconService";
    private static final long DEFAULT_FOREGROUND_SCAN_PERIOD = 1000L;
    private static final long DEFAULT_FOREGROUND_BETWEEN_SCAN_PERIOD = 1000L;
    public static final String IBEACON_FORMAT = "m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24";

    private static BeaconManager beaconManager;
    public static Collection<BeaconDto> beaconList = new ArrayList<>();


    public static final Region filterRegion = new Region("FDA50693-A4E2-4FB1", null, null, null);

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        if (beaconManager == null)
        {
            beaconManager = BeaconManager.getInstanceForApplication(this);
            beaconManager.setForegroundBetweenScanPeriod(DEFAULT_FOREGROUND_BETWEEN_SCAN_PERIOD);
            beaconManager.setForegroundScanPeriod(DEFAULT_FOREGROUND_SCAN_PERIOD);
            beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(IBEACON_FORMAT));
        }
        beaconManager.addRangeNotifier(rangeNotifier);
        beaconManager.startRangingBeacons(filterRegion);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        beaconManager.stopRangingBeacons(filterRegion);
        beaconManager.removeAllRangeNotifiers();
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    RangeNotifier rangeNotifier = new RangeNotifier() {
        @Override
        public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
            if (beacons.size() > 0) {
                beaconList.clear();
                Log.d(TAG, "didRangeBeaconsInRegion called with beacon count:  "+beacons.size());
                for (Beacon beacon: beacons) {
                    BeaconDto beaconDto = new BeaconDto();
                    beaconDto.setName(beacon.getBluetoothName());
                    beaconDto.setUuid(beacon.getIdentifier(0).toUuid().toString());
                    beaconDto.setMac(beacon.getBluetoothAddress());
                    beaconDto.setRssi(beacon.getRssi());
                    beaconDto.setAvgRssi(beacon.getRunningAverageRssi());
                    beaconDto.setMajor(beacon.getIdentifier(1).toByteArray());
                    beaconDto.setMinor(beacon.getIdentifier(2).toByteArray());
                    beaconList.add(beaconDto);
                }
                Beacon firstBeacon = beacons.iterator().next();
            }
        }

    };
}
